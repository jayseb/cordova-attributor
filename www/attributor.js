var exec = require('cordova/exec');

module.exports.getAttribution = function(arg0, success, error) {
	exec(success, error, 'attributor', 'getAttribution', [arg0]);
}