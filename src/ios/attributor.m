/********* attributor.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <iAd/iAd.h>

@interface attributor : CDVPlugin {
  // Member variables go here.
}


- (void)getAttribution:(CDVInvokedUrlCommand*)command;

@end

@implementation attributor

- (void)getAttribution:(CDVInvokedUrlCommand*)command
{
    // Check for iOS 10 attribution implementation
    if ([[ADClient sharedClient] respondsToSelector:@selector(requestAttributionDetailsWithBlock:)]) { 
    NSLog(@"iOS 10 call exists"); 
    [[ADClient sharedClient] requestAttributionDetailsWithBlock:^(NSDictionary *attributionDetails, NSError *error) { 
    // Look inside of the returned dictionary for all attribution details
    NSLog(@"Attribution Dictionary: %@", attributionDetails); 
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:attributionDetails
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];

        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
            [self.commandDelegate sendPluginResult:nil callbackId:command.callbackId];

        } else {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult*  pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

        }
    }];
    } else{
        [self.commandDelegate sendPluginResult:nil callbackId:command.callbackId];
    }

}

@end
